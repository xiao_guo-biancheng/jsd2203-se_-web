package com.example.boot21.mapper;

import com.example.boot21.entity.Emp;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface EmpMapper {
    //#{xxx}这种写法会自动找到下面方法中同名的变量,如果没有同名变量则会进入到对象里面
    //找到同名属性的get方法
    @Insert("insert into myemp values(null,#{name},#{sal},#{job})")
    void insert(Emp emp);

    //执行查询SQL语句的注解 查询到的数据会自动封装到Emp对象并且添加到List集合中
    @Select("select * from myemp")
    List<Emp> select();
    //执行删除SQL语句
    @Delete("delete from myemp where id=#{id}")
    void delete(int id);
    //执行修改SQL语句
    @Update("update myemp set name=#{name},sal=#{sal},job=#{job} where id=#{id}")
    void update(Emp emp);


}
