package com.example.boot21.mapper; 
 /*
 @Time: 2022/8/11 8:46
 @Author:GShuai
 @File:UserMapper.class
 @Software:IntelliJ IDEA
*/

import com.example.boot21.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserMapper {
    //查询回来的数据如果是一条或0条可以直接用自定义对象进行封装
    //如果查询回来的数据时0条或多条不能通过一个自定义对象接收,需要通过List集合接收
    @Select("select * from user where username=#{username}")
    User selectByUsername(String username);

    @Insert("insert into user values(null,#{username},#{password},#{nick})")
    void insert(User user);
}
