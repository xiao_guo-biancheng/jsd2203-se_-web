package com.example.boot21.controller;


import com.example.boot21.entity.Emp;
import com.example.boot21.mapper.EmpMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class EmpController {
     //自动装配注解,此注解会结合Spring框架和MyBatis生成一个Mapper接口的实现类并且实现了接口中的所有抽象方法
    //然后实例化了该对象赋值给了mapper变量
    @Autowired(required = false)
    EmpMapper mapper;

    @RequestMapping("/insert")
    public String insert(Emp emp){
        System.out.println("emp = " + emp);
        mapper.insert(emp);
        return "添加完成!";
    }

    @RequestMapping("/select")
    public String select(){
        List<Emp> list = mapper.select();

        String html = "<table border=1>";
        html+="<caption>员工列表</caption>";
        html+="<tr><th>id</th><th>名字</th><th>工资</th><th>工作</th><th>操作</th></tr>";
        //遍历集合 并把数据添加到表格中
        for (Emp emp: list) {
            html+="<tr>";
            html+="<td>"+emp.getId()+"</td>";
            html+="<td>"+emp.getName()+"</td>";
            html+="<td>"+emp.getSal()+"</td>";
            html+="<td>"+emp.getJob()+"</td>";
            html+="<td><a href='/delete?id="+emp.getId()+"'>删除</a></td>";
            html+="</tr>";
        }
        html+="</table>";
        return html;
    }
    @RequestMapping("/delete")
    public String delete(int id){
        mapper.delete(id);
        return "删除成功!<a href='/select'>返回列表页面</a>";
    }

    @RequestMapping("/update")
    public String update(Emp emp){
        mapper.update(emp);
        return "修改成功!<a href='/select'>返回列表页面</a>";
    }

}
