package com.example.boot21.controller; 
 /*
 @Time: 2022/8/11 14:16
 @Author:GShuai
 @File:AxiosController.class
 @Software:IntelliJ IDEA
*/

import com.example.boot21.entity.User;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AxiosController {
    @RequestMapping("/helloAxios")
    public String helloAxios(String info,String msg){
        return "请求成功!"+info+"msg="+msg;
    }


    //如果前端发出的是post请求并且参数为自定义的JS对象,在服务器接收参数时 需要使用
    //@RequestBody注解  否则接收不到参数
    @RequestMapping("/postAxios")
    public String postAxios(@RequestBody User user){
        return  "post请求成功!"+user.toString();
    }
}
