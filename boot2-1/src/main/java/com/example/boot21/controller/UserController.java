package com.example.boot21.controller; 
 /*
 @Time: 2022/8/11 8:42
 @Author:GShuai
 @File:UserController.class
 @Software:IntelliJ IDEA
*/

import com.example.boot21.entity.User;
import com.example.boot21.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    @Autowired(required = false)
    UserMapper mapper;

    @RequestMapping("/reg")
    public String reg(User user){
        System.out.println("user = " + user);
        User u = mapper.selectByUsername(user.getUsername());
        if(u!=null){
            return "用户名已存在!";
        }
        mapper.insert(user);
        return "注册完成！";
    }
    @RequestMapping("/login")
    public String login(User user) {
        System.out.println("user = " + user);
        // 通过用户的输入名查询出数据库的信息
        User us = mapper.selectByUsername(user.getUsername());
        if(us!=null){  //用户名存在
            // 拿用户输入的和查询到的密码比较
            if (us.getPassword().equals(user.getPassword())){
                return "登陆成功!<a href='/'>登陆成功</a>";
            }
            return "密码错误!<a href='/login.html'>重新登陆</a>";
        }
        return "用户名不存在! <a href='/login.html'>返回首页</a>";
    }

    @RequestMapping("/regA")
    public int regA(@RequestBody User user){
        User u =mapper.selectByUsername(user.getUsername());
        if(u!=null){
            return 2; //用户名已存在!
        }
        mapper.insert(user);
        return 1;  //注册成功
    }
    @RequestMapping("/loginA")
    public int loginA(@RequestBody User user){
        User u = mapper.selectByUsername(user.getUsername());
        if(u!=null){
          if (user.getPassword().equals(u.getPassword())){
              return 1; // 登陆成功
          }
            return 3;
        }
        return 2; //用户名不存在
    }
}
