package cn.tedu.boot41.mapper; 
 /*
 @Time: 2022/8/14 9:42
 @Author:GShuai
 @File:EmpMapper.class
 @Software:IntelliJ IDEA
*/

import cn.tedu.boot41.entity.Emp;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface EmpMapper {
    @Insert("insert into myemp values(null,#{name},#{sal},#{job})")
    void insert(Emp emp);

    @Select("select * from myemp")
    List<Emp> select();

    @Delete("delete from myemp where id=#{id}")
    void deleteById(int id);

    @Select("select * from myemp where id=#{id}")
    Emp selectById(int id);

    @Update("update myemp set name=#{name},sal=#{sal},job=#{job} where id=#{id}")
    void update(Emp emp);
}
