package cn.tedu.boot41.controller; 
 /*
 @Time: 2022/8/13 23:04
 @Author:GShuai
 @File:EmpController.class
 @Software:IntelliJ IDEA
*/

import cn.tedu.boot41.entity.Emp;
import cn.tedu.boot41.mapper.EmpMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class EmpController {
    @Autowired(required = false)
    EmpMapper mapper;


    @RequestMapping("/insert")
    public void insert(@RequestBody Emp emp){
        System.out.println("emp = " + emp);
        mapper.insert(emp);
    }

    @RequestMapping("/select")
    public List<Emp> select(){

        return mapper.select();
    }

    @RequestMapping("/delete")
    public void delete(int id){
        System.out.println("id = " + id);
        mapper.deleteById(id);
    }
    @RequestMapping("/selectById")
    public Emp selectById(int id){
        System.out.println("id = " + id);
        return mapper.selectById(id);
    }

    @RequestMapping("update")
    public void update(@RequestBody Emp emp){
        mapper.update(emp);
    }
}
