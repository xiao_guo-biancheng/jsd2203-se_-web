package cn.tedu.boot11.controller; 
 /*
 @Time: 2022/8/10 8:22
 @Author:GShuai
 @File:EmpController.class
 @Software:IntelliJ IDEA
*/

import cn.tedu.boot11.entity.Emp;
import cn.tedu.boot11.utils.DBUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@RestController
public class EmpController {

    @RequestMapping("/insert")
    public String insert(Emp emp){
        System.out.println("emp = " + emp);
        try (Connection conn = DBUtils.getConn()){
            String sql= "insert into myemp values(null,?,?,?)";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1,emp.getName());
            ps.setInt(2,emp.getSal());
            ps.setString(3,emp.getJob());
            // 执行插入sql语句
            ps.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return "添加完成!";
    }


    @RequestMapping("/select")
    public String select(){
        ArrayList<Emp> list = new ArrayList<>();
        // 查询所有数据
        try (Connection conn = DBUtils.getConn()){
            String sql="select * from myemp";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                int id = rs.getInt(1);
                String name = rs.getString(2);
                int sal = rs.getInt(3);
                String job = rs.getString(4);
                // 吧查询到的信息封装到emp中
                Emp emp =new Emp(id,name,sal,job);
                list.add(emp);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        String html = "<table border=1>";
        html+="<caption>员工列表</caption>";
        html+="<tr><th>id</th><th>名字</th><th>工资</th><th>工作</th><th>操作</th></tr>";
        // 遍历集合  并把数据 添加到表格中
        for (Emp emp:list){
            html+="<tr>";
            html+="<td>"+emp.getId()+"</td>";
            html+="<td>"+emp.getName()+"</td>";
            html+="<td>"+emp.getSal()+"</td>";
            html+="<td>"+emp.getJob()+"</td>";
            html+="<td><a href='/delete?id="+emp.getId()+"'>删除</a></td>";
            html+="</tr>";
        }
        html+="</table>";
        return html;
    }

    @RequestMapping("/delete")
    public String delete(int id){
        try (Connection conn = DBUtils.getConn()){
            String sql = "delete * from myemp where id=?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1,id);
            ps.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return  "删除完毕！<a href='/select'>返回列表页面</a>";
    }


    @RequestMapping("/update")
    public String update(Emp emp){
        try (Connection conn = DBUtils.getConn()){
            String sql="update myemp set name=?,sal=?,job=? where id=?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1,emp.getName());
            ps.setInt(2,emp.getSal());
            ps.setString(3,emp.getJob());
            ps.setInt(4,emp.getId());
            ps.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return  "修改完毕！<a href='/select'>返回列表页面</a>";
    }
}
