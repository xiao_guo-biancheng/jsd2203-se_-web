package cn.tedu.boot11.controller; 
 /*
 @Time: 2022/8/9 16:45
 @Author:GShuai
 @File:UserController.class
 @Software:IntelliJ IDEA
*/

import cn.tedu.boot11.entity.User;
import cn.tedu.boot11.utils.DBUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Controller
public class UserController {

    @RequestMapping("/user/reg")
    @ResponseBody
    public String reg(User user){
        System.out.println("user = " + user);
        // 获取数据库链接
        try (Connection conn = DBUtils.getConn()){
            String sql = "insert into user values(null,?,?,?)";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1,user.getUsername());
            ps.setString(2,user.getPassword());
            ps.setString(3,user.getNick());
            ps.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return "注册成功!";
    }

    @RequestMapping("/user/login")
    @ResponseBody
    public String login(User user){
        System.out.println("user = " + user);
        // 获取数据库链接
        try (Connection conn = DBUtils.getConn()){
            String sql = "select password from user where username=?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1,user.getUsername());
            ResultSet rs = ps.executeQuery();
            if (rs.next()){
                String pw=rs.getString(1);
                if (user.getPassword().equals(pw)){
                    return  "登陆成功！";
                }
                return "密码错误！";
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return "用户名不存在!";
    }

}
