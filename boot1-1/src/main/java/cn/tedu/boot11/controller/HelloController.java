package cn.tedu.boot11.controller;

import cn.tedu.boot11.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/*
 @Time: 2022/8/9 14:42
 @Author:GShuai
 @File:HelloController.class
 @Software:IntelliJ IDEA
*/

// 告诉SpringMVC框架当前类为控制器类，当接收到客户端动态请求时
// 能够找到此类控制器,并且从里面找到对应的方法
@Controller
public class HelloController {
    //localhost:8080/hello
    @RequestMapping("/hello")
    // 配置请求路径和方法的对应关系,当前客户端向localhost:8080/hello发请求时
    // 会自动找到此注解所修饰的方法
    @ResponseBody  // ResponseBody可以通过方法返回值的方式给客户端响应数据
    public String hello(){
        return "请求成功!";
    }

//    @RequestMapping("/reg")
//    @ResponseBody
//    public String reg(String username,String password,String nick){
//
//        return username+":"+password+":"+nick;
//    }

    @RequestMapping("/reg")
    @ResponseBody
    public String reg(User user){
        return user.toString();
    }
}
