package cn.tedu; 
 /*
 @Time: 2022/8/8 15:20
 @Author:GShuai
 @File:demo02.class
 @Software:IntelliJ IDEA
*/


import java.sql.*;

public class demo03 {
    public static void main(String[] args) throws SQLException {
        // 获取链接对象
        Connection conn = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/empdb?characterEncoding=utf8&serverTimezone=Asia/Shanghai&useSSL=false",
                "root","123456");
        // 创建执行sql语句的对象
        Statement s = conn.createStatement();
        //执行插入数据的SQL语句
        //s.executeUpdate("insert  into emp(name) values('Tom')");
        // 执行修改的数据SQL语句
        //s.executeUpdate("update emp set name='Jerry' where name ='Tom'");
        // 执行删除的数据SQL语句
        //s.executeUpdate("delete from emp where name = 'Jerry'");


        // 执行查询数据SQL语句
        ResultSet rs = s.executeQuery("select name,sal,job from emp");
        //遍历结果集对象
        while (rs.next()){
            String name =rs.getString("name");
            double sal =rs.getDouble("sal");
            String job =rs.getString("job");
            System.out.println(name+":"+sal+":"+job);
        }
        // 关闭资源
        conn.close();
        System.out.println("执行完成!");
    }
}
