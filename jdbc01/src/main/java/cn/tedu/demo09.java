package cn.tedu; 
 /*
 @Time: 2022/8/9 9:55
 @Author:GShuai
 @File:demo09.class
 @Software:IntelliJ IDEA
*/


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class demo09 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入用户名:");
        String username = scanner.nextLine();
        System.out.println("请输入密码:");
        String password = scanner.nextLine();
        System.out.println("请输入昵称:");
        String nick = scanner.nextLine();
        try(Connection connection = DBUtils.getConn()) {
            String selectSql = "select id from user where username=?";
            PreparedStatement preparedStatement = connection.prepareStatement(selectSql);
            preparedStatement.setString(1,username);
            ResultSet resultSet = preparedStatement.executeQuery();
            // 执行查询
            if (resultSet.next()){
                System.out.println("用户已存在!");
                return;
            }
            //执行插入sql语句
            String insertSql = "insert into user values(null,?,?,?)";
            PreparedStatement preparedStatement1 = connection.prepareStatement(insertSql);
            preparedStatement1.setString(1,username);
            preparedStatement1.setString(2,password);
            preparedStatement1.setString(3,nick);
            preparedStatement1.executeUpdate();
            System.out.println("注册成功!");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
