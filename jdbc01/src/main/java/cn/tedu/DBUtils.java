package cn.tedu; 
 /*
 @Time: 2022/8/8 16:22
 @Author:GShuai
 @File:DBUtils.class
 @Software:IntelliJ IDEA
*/


import com.alibaba.druid.pool.DruidDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class DBUtils {
    // 声明全局变量
    private static DruidDataSource ds;
    static {
        // 创建表示连接池对象
        ds = new DruidDataSource();
        // 设置连接数据库的信息
        ds.setUrl("jdbc:mysql://localhost:3306/empdb?characterEncoding=utf8&serverTimezone=Asia/Shanghai&useSSL=false");
        ds.setUsername("root");
        ds.setPassword("123456");
        // 设置初始连接数量
        ds.setInitialSize(3);
        // 设置最大连接数量
        ds.setMaxActive(5);
    }
    public static Connection getConn() throws SQLException {

        // 从连接池中获取链接  异常抛出
        Connection conn = ds.getConnection();
        System.out.println("连接对象"+conn);
        return conn;
    }
}
