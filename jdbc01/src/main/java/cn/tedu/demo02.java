package cn.tedu; 
 /*
 @Time: 2022/8/8 15:20
 @Author:GShuai
 @File:demo02.class
 @Software:IntelliJ IDEA
*/


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class demo02 {
    public static void main(String[] args) throws SQLException {
        Connection conn = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/empdb?characterEncoding=utf8&serverTimezone=Asia/Shanghai&useSSL=false",
                "root","123456");
        System.out.println("链接:"+conn);
        Statement s= conn.createStatement();
        s.execute("drop table jdbct1");
        conn.close();
        System.out.println("执行完成!");
    }
}
