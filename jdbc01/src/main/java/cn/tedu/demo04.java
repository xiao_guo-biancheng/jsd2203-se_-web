package cn.tedu; 
 /*
 @Time: 2022/8/8 16:25
 @Author:GShuai
 @File:demo04.class
 @Software:IntelliJ IDEA
*/


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class demo04 {
    public static void main(String[] args) {

        try (Connection conn = DBUtils.getConn()) {
            // 创建执行SQl语句的对象
            Statement s = conn.createStatement();
            // 执行SQL语句
            ResultSet rs = s.executeQuery("select name,sal from emp");
            //遍历结果集
            while (rs.next()) {
//                String name = rs.getString("name");
//                double sal = rs.getDouble("sal");
                //通过查询回来字段的位置获取数据
                String name = rs.getString(1);
                double sal = rs.getDouble(2);
                System.out.println(name + ":" + sal);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
