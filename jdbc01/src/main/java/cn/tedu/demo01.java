package cn.tedu; 
 /*
 @Time: 2022/8/8 14:23
 @Author:GShuai
 @File:demo01.class
 @Software:IntelliJ IDEA
*/


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class demo01 {
    public static void main(String[] args) throws SQLException {
        // 1.建立链接
        Connection conn = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/empdb?characterEncoding=utf8&serverTimezone=Asia/Shanghai&useSSL=false",
                "root","123456");
        System.out.println("链接:"+conn);
        // 2.创建执行sql语句的对象
        Statement s= conn.createStatement();
        // 3.执行sql语句
        s.execute("create table jdbct1(id int)");
        // 4.关闭资源
        conn.close();
        System.out.println("执行完成!");
    }
}
