package cn.tedu; 
 /*
 @Time: 2022/8/8 17:33
 @Author:GShuai
 @File:demo06.class
 @Software:IntelliJ IDEA
*/


import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class demo06 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("请输入用户名:");
        String username= scan.nextLine();
        System.out.println("请输入密码:");
        String password= scan.nextLine();
        System.out.println("请输入昵称:");
        String nick= scan.nextLine();
        // 获取链接
        try(Connection conn = DBUtils.getConn()) {
            Statement s = conn.createStatement();
            //执行插入SQL语句
            s.executeUpdate("insert into user values(null,'"+username+"','"+password+"','"+nick+"')");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
