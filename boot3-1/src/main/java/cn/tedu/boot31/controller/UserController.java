package cn.tedu.boot31.controller;

import cn.tedu.boot31.entity.User;
import cn.tedu.boot31.mapper.UserMapper;
import com.sun.org.apache.regexp.internal.RE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    @Autowired(required = false)
    UserMapper mapper;

    @RequestMapping("/reg")
    public int reg(@RequestBody User user){
        User u = mapper.selectByUsername(user.getUsername());
        if(u!=null){
            return 2; //代表用户名已经存在
        }
        mapper.insert(user);
        return 1; // 注册成功
    }
    @RequestMapping("login")
    public int login(@RequestBody User user){
        User u = mapper.selectByUsername(user.getUsername());
        if (u!=null){
            if(user.getPassword().equals(u.getPassword())) {
                return 1; //登陆成功
            }
            return 3; // 密码错误
        }
        return 2; //用户名不存在

    }

}
