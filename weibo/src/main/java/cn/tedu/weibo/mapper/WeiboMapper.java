package cn.tedu.weibo.mapper; 
 /*
 @Time: 2022/8/22 14:57
 @Author:GShuai
 @File:WeiboMapper.class
 @Software:IntelliJ IDEA
*/

import cn.tedu.weibo.eneity.Weibo;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface WeiboMapper {
    @Insert("insert into weibo values(null,#{content},#{created},#{userId},#{nick})")
    void insert(Weibo weibo);

    @Select("select * from weibo")
    @Result(column = "user_id",property = "userId")
    List<Weibo> select();

    @Delete("delete from weibo where id=#{id}")
    void deleteById(int id);
}
