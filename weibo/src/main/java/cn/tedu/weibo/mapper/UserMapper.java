package cn.tedu.weibo.mapper; 
 /*
 @Time: 2022/8/22 8:27
 @Author:GShuai
 @File:UerMapper.class
 @Software:IntelliJ IDEA
*/

import cn.tedu.weibo.eneity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserMapper {
    @Select("select * from user where username = #{username}")
    User selectByUsername(String username);

    @Insert("insert into user values(null,#{username},#{password},#{nick})")
    void insert(User user);
}
