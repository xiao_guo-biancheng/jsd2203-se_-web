package cn.tedu.weibo.Controller; 
 /*
 @Time: 2022/8/22 15:00
 @Author:GShuai
 @File:WeiboController.class
 @Software:IntelliJ IDEA
*/

import cn.tedu.weibo.eneity.User;
import cn.tedu.weibo.eneity.Weibo;
import cn.tedu.weibo.mapper.WeiboMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

@RestController
public class WeiboController {
    @Autowired(required = false)
    WeiboMapper mapper;

    @RequestMapping("insert")
    public int insert(Weibo weibo, HttpSession session){
        System.out.println("weibo = " + weibo + ", session = " + session);
        //得到登录成功时的用户对象
        User user = (User) session.getAttribute("user");
        if (user==null){
            return 2; //代表未登录
        }
        weibo.setCreated(new Date());
        weibo.setUserId(user.getId());
        weibo.setNick(user.getNick());
        mapper.insert(weibo);
        return 1;//发布成功
    }

    @RequestMapping("/select")
    public List<Weibo> select(){
        return mapper.select();
    }

    @RequestMapping("delete")
    public void delete(int id){
        mapper.deleteById(id);
    }
}
