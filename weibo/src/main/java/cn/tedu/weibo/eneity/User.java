package cn.tedu.weibo.eneity; 
 /*
 @Time: 2022/8/22 8:24
 @Author:GShuai
 @File:User.class
 @Software:IntelliJ IDEA
*/


public class User {
    private Integer id;
    private String username;
    private String password;
    private String nick;
    private Boolean rem;  //remember 是否记住密码

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public Boolean getRem() {
        return rem;
    }

    public void setRem(Boolean rem) {
        this.rem = rem;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", nick='" + nick + '\'' +
                ", rem=" + rem +
                '}';
    }
}
