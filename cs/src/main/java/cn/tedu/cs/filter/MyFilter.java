package cn.tedu.cs.filter;/*
@Time: 2022/8/22 17:56
@Author:GShuai
@File:${NAME}.class
@Software:IntelliJ IDEA
*/


import cn.tedu.cs.entity.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;



@WebFilter(filterName = "MyFilter",urlPatterns = {"/insertProduct.html","/insertBanner.html"})
public class MyFilter implements Filter {
    public void init(FilterConfig config) throws ServletException{

    }

    public void destroy() {
    }

    @Override
    // 此方法是请求到资源之前和之后调用的方法
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
       //因为使用后子类型的方法，所以需要将父类类型强转成子类类型
        HttpServletRequest ht = (HttpServletRequest) request;
        HttpServletResponse hs = (HttpServletResponse) response;
        chain.doFilter(request, response);   // 代表运行执行请求的资源
        //从请求对象中得到Session 再从Session中得到User
        User user = (User) ht.getSession().getAttribute("user");
        if (user==null){//没登录
            //如果没登录则重定向到登录页面
            hs.sendRedirect("/login.html");
        }else{//代表登录了
            chain.doFilter(request, response);//代表运行执行请求的资源
        }
    }
}
