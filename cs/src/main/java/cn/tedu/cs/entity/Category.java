package cn.tedu.cs.entity; 
 /*
 @Time: 2022/8/17 21:22
 @Author:GShuai
 @File:Category.class
 @Software:IntelliJ IDEA
*/


public class Category {
    private Integer id;
    private String  name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
