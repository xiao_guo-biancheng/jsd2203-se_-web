package cn.tedu.cs.mapper; 
 /*
 @Time: 2022/8/17 21:16
 @Author:GShuai
 @File:UserMapper.class
 @Software:IntelliJ IDEA
*/

import cn.tedu.cs.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserMapper {
    @Select("select * from user where username=#{username}")
    User selectByUsername(String username);

}
