package cn.tedu.cs.mapper; 
 /*
 @Time: 2022/8/17 21:35
 @Author:GShuai
 @File:BannerMapper.class
 @Software:IntelliJ IDEA
*/

import cn.tedu.cs.entity.Banner;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface BannerMapper {
    @Select("select * from banner")
    List<Banner> select();

    @Insert("insert into banner values(null,#{url})")
    void insert(String url);


    //删除轮播图 先查后删
    @Select("select url from banner where id=#{id}")
    String selectUrlById(int id);
    // 删除
    @Delete("delete from banner where id=#{id}")
    void deleteById(int id);
}
