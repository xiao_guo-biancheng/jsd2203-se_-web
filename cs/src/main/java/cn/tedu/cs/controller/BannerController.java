package cn.tedu.cs.controller; 
 /*
 @Time: 2022/8/17 21:39
 @Author:GShuai
 @File:BannerController.class
 @Software:IntelliJ IDEA
*/

import cn.tedu.cs.entity.Banner;
import cn.tedu.cs.mapper.BannerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.util.List;

@RestController
public class BannerController {
    @Autowired(required = false)
    BannerMapper mapper;

    // 读取application.properties 文件中的内容  并赋值给变量
    @Value("G:/files")
    private String dirPath;

    @RequestMapping("/banner/select")
    public List<Banner> select(){
        return mapper.select();
    }
    @RequestMapping("/banner/insert")
    public void insert(String url){
        mapper.insert(url);
    }

    //删除轮播图
    @RequestMapping("banner/delete")
    public void delete(int id){
        // 通过商品的id查询出轮播图的路径
        String url= mapper.selectUrlById(id);
        // 得到完整的文路径
        String filePath= dirPath+"/"+url;
        // 删除文件
        new File(filePath).delete();
        mapper.deleteById(id);
    }
}
