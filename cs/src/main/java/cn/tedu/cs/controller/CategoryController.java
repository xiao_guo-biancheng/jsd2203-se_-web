package cn.tedu.cs.controller; 
 /*
 @Time: 2022/8/17 21:24
 @Author:GShuai
 @File:CategoryController.class
 @Software:IntelliJ IDEA
*/

import cn.tedu.cs.entity.Category;
import cn.tedu.cs.mapper.CategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CategoryController {
    @Autowired(required = false)
    CategoryMapper mapper;

    @RequestMapping("/category/select")
    public List<Category> select(){
        return mapper.select();
    }

    @RequestMapping("/category/delete")
    public void delete(int id){
        mapper.deleteById(id);
    }

    @RequestMapping("/category/insert")
    public void insert(String name){
        mapper.insert(name);
    }
}
