package cn.tedu.cs.controller; 
 /*
 @Time: 2022/8/17 21:12
 @Author:GShuai
 @File:UserController.class
 @Software:IntelliJ IDEA
*/

import cn.tedu.cs.entity.User;
import cn.tedu.cs.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
public class UserController {
    @Autowired(required = false)
    UserMapper mapper;
    @RequestMapping("/login")
    public int login(@RequestBody User user, HttpSession session){
        System.out.println("user = " + user);
        User u = mapper.selectByUsername(user.getUsername());
        if(u!=null){
            if(user.getPassword().equals(u.getPassword())){
                session.setAttribute("user",u);
                return 1; // 成功
            }
            return 3;  // 密码错误
        }
        return 2;  //用户名不存在
    }

    @RequestMapping("currentUser")
    public User currentUser(HttpSession session){
        User user=(User)session.getAttribute("user");
        return user;
    }


    @RequestMapping("/logout")
    public void logout(HttpSession session){
        session.removeAttribute("user");
    }
}
